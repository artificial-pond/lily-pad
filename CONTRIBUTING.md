# Contributing to Lily Pad

We welcome contributions from everyone! By contributing to this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md) and the terms of the [LICENSE](LICENSE).

## How to Contribute

1. Fork this repository and clone it to your local machine.
2. Create a new branch from the `main` branch with a descriptive name that summarizes the change you want to make.
3. Make your changes and commit them with clear commit messages.
4. Push your changes to your forked repository.
5. Open a pull request to the `main` branch of the Lily Pad repository.

## Code of Conduct

All contributors are expected to follow our Code of Conduct, which can be found in the [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md) file.

## License Compliance

By contributing to the Lily Pad project, you agree to abide by the terms of the [LICENSE](LICENSE). Before submitting a pull request, please ensure that your contribution is in compliance with the license.

## Branch Protection

We use the `main` branch as the default branch for the Lily Pad project. To ensure that all software releases can be traced back to a specific branch, we have implemented the following branch protection measures:

- The `main` branch is protected from deletion.
- The `main` branch is protected from changes that affect previous changes (e.g., force pushes).
- Direct changes to the `main` branch by unauthorized users are not allowed.

## Clean Commit History

We encourage contributors to keep a clean commit history. This means that each commit should be a small, logical change that can be easily understood on its own. To keep the commit history clean, please:

- Keep commits focused on a single change or feature.
- Use descriptive commit messages that summarize the change.
- Squash related commits into a single commit before submitting a pull request.

Thank you for contributing to the Lily Pad project!
