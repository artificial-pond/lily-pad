# Contributors Decision

## Requirements

- It shall be easy to add new contributors.
- It shall be easy to find out who contributed to the software.
- It shall be possible to use freely chosen names for contributors. (e.g. username, real name, alias, ...)
- It shall be possible for us to deny contributors with inappropriate names.
- It shall be possible to verify that a contributor contributed to the software.
- Contributors that are inactive or whose contributions are completely replaced by other contributors shall remain acknowledged.

## Options

- Add contributors to the license file.
- Add contributors to a notice file.
- Add contributors to the README file.
- Add contributors to the wiki.
- Add contributors to the website.

## Decision

We decided to add contributors to a notice file as recommended by the Apache 2.0 license.
Users that contribute to the software can add themselves to the notice file via a pull request.

## Consequences

- [ ] We need to add a notice file to the repository.
- [ ] We need to take care that our workflow verifies a connection between a contributor and their contributions. (e.g. Pull requests, commits, ...)
- [ ] We need to take care that our workflow verifies that a contributor is not added twice.
- [ ] We need to take care that our workflow verifies that a contributor is not added to the notice file if they are not a contributor.
- [ ] We need to take care that our workflow verifies that a contributor remains in the notice file if they are inactive or their contributions are completely replaced by other contributors.
