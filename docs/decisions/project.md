# Project Decision

## Requirements

- It shall be a game.
- It shall be a simple project.
- It shall be a project to learn how to develop software.
- It shall be a project that can be completed by a team of two people.
- It shall be a project that can be completed in a reasonable amount of time.
- It shall be a project that covers following aspects:
  - Decision making
  - Requirements
  - Architecture
  - Design
  - Implementation
  - Testing
  - Documentation
  - Deployment
  - Maintenance / Issue Management
- It shall be a project that can be used as a template for future projects.
- It shall be a project that can be extended in the future.

## Options

- To-Do List: Create a simple to-do list application that allows users to create, edit, and delete tasks. The application can have features such as due dates, reminders, and the ability to categorize tasks. Users can mark tasks as complete and view their progress.

- Trivia Game: Develop a trivia game where users answer multiple-choice questions from various categories such as sports, history, geography, or pop culture. The game can have different levels of difficulty and a time limit for each question. Users can earn points for correct answers and compete with other players on a leaderboard.

- Memory Game: Create a classic memory game where users have to match pairs of cards with identical images or text. The game can have different levels of difficulty, with more cards to match at higher levels. Users can compete against a timer or try to beat their high score.

- Weather App: Develop a simple weather application that displays the current weather conditions and forecast for a specific location. The app can include features such as temperature, humidity, wind speed, and precipitation. Users can search for different cities and switch between Celsius and Fahrenheit units.

- Online Quiz: Build an online quiz platform where users can create and take quizzes on various topics. The platform can allow users to choose the type of questions, such as multiple-choice, true/false, or fill-in-the-blank. Users can share their quizzes with others and see how they perform on leaderboards.

## Decision

We decided to go with the Memory Game.

## Consequences

- [x] We need to find a suitable name for the project.
- [x] We need to start the project.
