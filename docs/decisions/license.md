# License decision

## Requirements

- It shall allow others to copy and use the source code and software for private and commercial purposes.
- It shall protect us from liability and being responsible for any damage caused by the software.
- In case others contribute to the software, it shall protect us from claims by others that they contributed to the software.

## Options

- [MIT](https://choosealicense.com/licenses/mit/)
- [Apache 2.0](https://choosealicense.com/licenses/apache-2.0/)

## Decision

Apache 2.0 includes a patent grant and a disclaimer of warranty, which is important for us.
MIT on the other hand is more permissive and allows others to use the software without any restrictions.

Thus, we decided to use Apache 2.0.

## Consequences

- [x] We need to add a license file to the repository.
- [x] We need to decide how to acknowledge contributors.
- [ ] We need to distribute the license with the software.
