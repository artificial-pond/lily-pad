# Project Name Decision

## Requirements

- It shall be short
- It shall be easy to pronounce
- It shall be easy to remember
- It shall be easy to type
- It shall be easy to search for

## Options

- Pond Pairs
- Memory Lilies
- Lily Pad Match
- Fishy Match
- Water Memory
- Pond Recall
- Ripple Match
- Froggy Memory
- Lillypad Remembrance
- Aquatic Recall

## Decision

Lily Pad Match is a good name for the game. It is easy to pronounce, easy to remember, easy to type and easy to search for. Allthough since it is a bit long, we decided to shorten it to Lily Pad.

## Consequences

- [x] We need to name the repository for the project: Lily Pad.
