# Git Workflow Decision

This document contains the decision made about the git workflow. 

## Requirements
- It shall keep the history clean and provide structure without adding too much complexity. (Standardization)
  - It shall provide a way to work on features without affecting the main branch. (Isolation)
- It shall be possible to define roles and responsibilities. (Accountability)
- It shall provide mechanism to review code. (Quality control)
  - It shall be possible to enforce feature completion before merging. (no unfinished work)
- It shall help to make it easy to collaborate on the project. (Collaboration)
- It shall be possible to easily track changes and rejected contributions. (Traceability / Version control)
- It shall be possible to easily revert changes. (Revertability)

## Options

- [Centralized Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/centralized-workflow)
- [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/)
- [GitHub Flow](https://guides.github.com/introduction/flow/)
- [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
- [Atlassian Git Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows)
- [Feature Branch Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
- [Forking Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow)

## Decision

We decided to use GitLab Flow as our git workflow It is less complex than Git Flow and provides a good balance between structure and flexibility. It also provides isolation when working on features e.g.:
If the scope of the feature is extensive, the feature branch can be used as a base for sub tasks. This way the feature branch can be merged into the main branch when the feature is complete.

## Consequences

- [ ] We need to add a file describing the guidelines for contributors and maintainers. (Specifically **maintainers** have to be familiar with the workflow and act accordingly.)
