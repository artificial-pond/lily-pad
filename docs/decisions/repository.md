# Repository Decision

## Requirements

- It shall be possible to store the source code and other files in a central location.
- It shall be possible to share the source code and other files with others.
- It shall be possible to track changes to the source code and other files.
- It shall be possible to revert changes to the source code and other files.
- It shall be possible to view the history of the source code and other files.
- It shall be possible to automatically build and test the software from the source code.
- It shall support us to automate the release process (CI/CD).

## Options

- [GitHub](github.com)
- [GitLab](gitlab.com)
- [Bitbucket](bitbucket.org)

## Decision

GitHub is a popular choice for open source projects and is used by many companies. It also has the capatibility to automatically build and test the software from the source code.
However, GitLab is a good alternative to GitHub and has the same capatibility. Additionally there is already knowledge about GitLab in the team - including the CI/CD pipelines, administration and usage of GitLab.

Thus, we decided to use GitLab.

## Consequences

- [x] We need to create a GitLab repository for the project: [Lily Pad](https://gitlab.com/artificial-pond/lily-pad).
