# Overview of the decisions made in the project

This document contains an overview of the decisions made in the project.

Table of Decisions

| Section | Decision | Link |
| --- | --- | --- |
| License | Apache 2.0 | [License](license.md) |
| Contributors | Notice File | [Contributors](contributors.md) |
| Repository | GitLab | [Repository](repository.md) |
| Project Name | Lily Pad | [Lily Pad](https://gitlab.com/artificial-pond/lily-pad) |
