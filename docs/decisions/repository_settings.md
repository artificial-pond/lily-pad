# Repository settings Decision

## Requirements

- It shall be possible to contribute to this project by everyone.
- It shall be possible to verify that any contribution is in compliance with the license.
- It shall be possible to verify that any contribution is in compliance with the code of conduct.
- It shall be possible to trace all software releases back to a specific branch.
  - It shall be possible to protect this branch from deletion.
  - It shall be possible to protect this branch from changes that affect previous changes.
  - It shall be possible to protect this branch from direct changes by unauthorized users.
- It shall be possible that the change history is always straight forward and not "ugly".

## Options

- Guidelines for contributors and maintainers.
- Settings enforcement by GitLab
- Settings enforcement by CI/CD

## Decision

We decided to use a combination of guidelines for contributors and maintainers and settings enforcement by GitLab and CI/CD.

## Consequences

- [ ] We need to add a file describing the guidelines for contributors and maintainers.
- [ ] We need to configure GitLab repository settings.
- [ ] We need to configure GitLab CI/CD settings.
